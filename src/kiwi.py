from dagster_kiwitcms.api import KiwiTCMSBaseAction

kiwi_params = {
    "classification": "iperf3",
    "product": "Iperf3 Measure Bandwidth",
    "product_description": "Iperf3 Measure Bandwidth",
    "plantype": "Integration",
    "testplan": "Iperf3 Bandwidth Testing",
    "iperf_summary": "iPerf Test completed",
    # These are defaults from the Stackstorm Kiwi actions
    "testcase_status": "CONFIRMED",
    "testcase_priority": "P1",
    "testcase_category": "--default--",
    "testrun_manager": "admin",
}

class KiwiTcms:
    def __init__(self, creds):
        self.creds = creds
        self.kiwi  = KiwiTCMSBaseAction(creds)
        pass

    def setup(self, status):
        kiwitcms = self.kiwi
        kiwidata = status["kiwidata"]
        params   = status["params"];
        
        result = kiwitcms.get_classification(name=kiwi_params["classification"])
        kiwidata["classification"] = result["id"]

        result = kiwitcms.ensure_product(name=kiwi_params["classification"],
                              description=kiwi_params["product_description"],
                              classification=kiwidata["classification"])
        kiwidata["product"] = result["id"]

        result = kiwitcms.ensure_version(product=kiwidata["product"],
                              value=params["iperf3_version"])
        kiwidata["version"] = result["id"]
    
        result = kiwitcms.ensure_plantype(name=kiwi_params["plantype"])
        kiwidata["plantype"] = result["id"]
    
        result = kiwitcms.ensure_testplan(name=kiwi_params["testplan"],
                              text=kiwi_params["testplan"] +
                              " version " + params["iperf3_version"],
                              type=kiwidata["plantype"],
                              product=kiwidata["product"],
                              product_version=kiwidata["version"])
        kiwidata["testplan"] = result["id"]

        result = kiwitcms.ensure_build(name=params["iperf3_version"],
                              version=kiwidata["version"])
        kiwidata["build"] = result["id"]
        
        result = kiwitcms.ensure_testcase(summary=kiwi_params["iperf_summary"],
                              text=kiwi_params["iperf_summary"],
                              testplan=kiwidata["testplan"],
                              status_name=kiwi_params["testcase_status"],
                              category=kiwi_params["testcase_category"],
                              priority=kiwi_params["testcase_priority"],
                              product_id=kiwidata["product"])
        kiwidata["testcase_iperf"] = result["id"]

        summary = "Testing Iperf3 Meansure Bandwidth (version=%s))" % (params["iperf3_version"])
        result = kiwitcms.create_testrun(summary=summary,
                              manager=kiwi_params["testrun_manager"],
                              build=kiwidata["build"],
                              plan=kiwidata["testplan"])
        kiwidata["testrun"] = result["id"]

        result = kiwitcms.add_testcase_to_testrun(run_id=kiwidata["testrun"],
                              case_id=kiwidata["testcase_iperf"],
                              status="BLOCKED")
        kiwidata["execution_iperf"] = result["id"]

    def update_execution(self, status, execution, result):
        self.kiwi.update_test_execution(execution_id=status["kiwidata"][execution],
                          status=result)
        pass

    def add_attachment(self, status, filename, content):
        self.kiwi.add_attachment_to_testrun(run_id=status["kiwidata"]["testrun"],
                          filename=filename,
                          content=content)
        pass