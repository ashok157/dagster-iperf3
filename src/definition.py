from ssh_client import SSHClient
from kiwi import KiwiTcms
from emulab_base import EmulabBaseAction

from dagster import op, job, Out
from dagster import Output, get_dagster_logger, job, op, Out
from dagster import resource

import os
import sys
import json

def update_execution(context, status, execution, result):
    kiwitcms = context.resources.kiwitcms
    kiwitcms.update_execution(status, execution, result)
    pass

def add_attachment(context, status, filename, content):
    kiwitcms = context.resources.kiwitcms
    kiwitcms.add_attachment(status, filename, content)
    pass

def create_status_blob(p):
    blob = {
        "kiwidata"        : {},
        "params"       : p
    }
    return blob

@op(config_schema={"params" : dict},
    out={"status": Out()})
def start_run(context):
    return create_status_blob(context.op_config["params"])

@op(tags={"dagster/priority": "0", "runone" : "runone"},
    required_resource_keys={"ssh_client"})
def createKeyfile(context, status):
    fname = "/tmp/id_rsa_perexpt"
    ssh_client = context.resources.ssh_client
    
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write(ssh_client.privKey() + "\n")
        f.close()
        pass
    
    status["keyfile"] = fname
    return status;

#
# Initialize the TCMS stuff
#
@op(required_resource_keys={"ssh_client", "kiwitcms"}, out={"status": Out()})
def setup_tcms(context, status):
    kiwitcms = context.resources.kiwitcms
    kiwitcms.setup(status)
    context.log.info(str(status["kiwidata"]))
    context.log.info("----- My Status Object -----")
    context.log.info(status)
    return status

@op(required_resource_keys={"emulab_api"})
def start_experiment(context, status):
    params = status["params"]
    portal = context.resources.emulab_api
    
    result = portal.startExperiment(name=params["name"],
                        proj=params["proj"],
                        profile=params["profile"],
                        bindings={"nodes": 2, "numberLans": 1},
                        max_wait_time=3600,
                        interval=30,
                        errors_are_fatal=True,
                        wait_for_status=True,
                        wait_for_execute_status=True)
    
    context.log.info(result)

    if result["result_code"] != 0:
        context.log.error(result.error)
        raise Exception("Unable to start experiment")

    context.log.info(type(context))

    return Output(
        result,
        metadata={
            "text_metadata": "EXPERIMENT_STARTED",
        },
    )

@op(required_resource_keys={"emulab_api"})
def experiment_status(context, params, status):
    output = params["error"]
    curr_kiwi_data = status['kiwidata']
    status = status["params"]

    status['kiwidata'] = curr_kiwi_data
    result_code = params["result_code"]
    portal = context.resources.emulab_api
    expr_params = params["expr_params"]
    if expr_params["wait_for_status"]:
            status_params = dict(**expr_params,asjson=True)
            ret = portal.experimentStatus(status_params)
            if expr_params["errors_are_fatal"] and ret["status"]["status"] == "failed":
                print("startExperiment error (failure_code=%r): %r" % (
                    ret["status"].get("failure_code","unknown"),output),
                    file=sys.stderr)
                exit(result_code)
            ret["params"] = expr_params
            get_dagster_logger().info("End of the function of startExperiment")
            parsed_data = json.loads(json.dumps(ret))

            server_exp_addr = parsed_data['manifest_facts']['urn:publicid:IDN+emulab.net+authority+cm']['nodes']['node-0']['links']['lan-0']['address']
            client_exp_addr = parsed_data['manifest_facts']['urn:publicid:IDN+emulab.net+authority+cm']['nodes']['node-1']['links']['lan-0']['address']

            host_server = parsed_data['status']['aggregate_status']['urn:publicid:IDN+emulab.net+authority+cm']['nodes']['node-0']['ipv4']
            host_client = parsed_data['status']['aggregate_status']['urn:publicid:IDN+emulab.net+authority+cm']['nodes']['node-1']['ipv4']
            status["server_exp_addr"] = server_exp_addr
            status["client_exp_addr"] = client_exp_addr
            status["host_server"] = host_server
            status["host_client"] = host_client
            status["iperf3_expr_terminate"] = True

            return Output(status, metadata={"text_metadata": "EXPERIMENT_CREATED",},)
    else:
        raise Exception("wait_for_status key is mandatory")

@op(out={"status": Out(dict)})
def build_config(context, status):
    server_output_path = status.get("server_output_path", "/tmp/server_output.json")
    client_output_path = status.get("client_output_path","/tmp/client_output.json")

    status['server_output_path'] = server_output_path
    status['client_output_path'] = client_output_path
    status["get_server_output"] = f"cat {server_output_path}"
    status["get_client_output"] = f"cat {client_output_path}"
    status["kill_command"] = "pkill iperf3"
    status["check_status"] = "pgrep iperf3"
    status["private_key"] = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA4yNAuqEzBZLY5VJO6X+vfQp8MfFwLPU3fM4a6Lm+w1IPkRez\nK3wgLvcUhuCBsJmCOI+gu+ApChu1WzcOS7yy4hfzIXXAu0haCjTGQAHE5Lj6qGzR\nDG+zNfUwez9QfDOPjBLk29/zIm3OFFBmYEd8wcDZNn/oMU/CIMrcGowqzNJXGmvC\n7CAFejOrADXr1ClyfdM81Jp/aAlNJRUz4g1RrE7W2jot7+hsNbucO5/CAanx4hep\nWZhS83RSINMJ+RiOSX4SEdupNitlb8YDmYUhiblU/rm2BH1srJjKQdhmPl5J4ftn\nc7SVg6a0ODWhESJRrX1TfA7Hu8Q3Yj0LzVEzgwIDAQABAoIBACqQO5v9gysjNoPa\npbD53W/IHDfbr5KtLQW1NbOwwv+lDAe7cjbO3tUONgmEXqkySwXAXHV4bxhW0uhR\n69rjdH4L0OLPRxuRVlYdR51M95GI3GfdZFs5P1L4LGqcFcAAhYDRNZPCZrRn8Ts7\nScP3AXjpY4rj8yXydUVh3lcXNxoEYiGe6BQJMcz89LVrv0si9KF+vMzgFgesOxZ7\nXZpk3jS0JaGWc4nALHidO3idqudXD0l8iW57AhdRDmF7EXA8sFnCUrl7wgcEiXct\nbuh7pm1u2eSguwuxmp6jf0dSmwmFiyTr+lcE446VLQ7PiGif13W3dKLXkJE3LyRp\nmt4EtNECgYEA+di0vlBTthoelNoNhh9rMbg+BYe86dNSSb0t6ND5oSi46kbiy++r\nV3h0L+oIPvDnfJV0j7fv0vEBDy1oDlJRnviX5EWFMr+bXyMslt1/I6+uqq/ZfwSZ\nntaQbmw73dqtaCy/JYj2whR07hieyFHzeTWgbQ5b391jrLY0X4fBFwkCgYEA6Ltd\n4+H/wmqNPp9iT6zRGhg0ok6JGcPx1+3DD/8T4q0vcS1taeBPS4u8ckNEruHGd99u\nx8Wgu9Qbt8xC4OaU5OJSda0vWLNrOQWE7p9pgizlQx1Fp4i0SpGYa/OW2vm4/1cT\nNTOR5J8vh7aPC8XJI9qLvguOMFTId+tzRP3X7SsCgYB+E5PJgaFsgCu0c4BlkQ83\nrMBnbAKxf7jDixpSpLmDUJhXaGPkA1JUNm1zDEcS89/sZ0LFj00PBhzBuukKaRHV\nBZ8oSsiXPkc6L/a9PpJ8wJeEfG5lgT3AAIgroKRpJINi0Um3uDDE6SvERlpBvdD9\n9ki5H4jPI9m7espc9pVCeQKBgQCpgATiuq+ediIG0Og2JSRpv+VdrJs3s7kIzba6\niMl/1yhtYmuEqAS4fb4y7gEw6Wi113oEb1AgXFwPoem3iNg6Vwc9g3OvQ4U9E2L8\ntAeQ8ofLOJ+JZAwDH0UGonNlamktNsrbfYKtIq7oRbKrHVLgFQSf1iSp2KXkiQv7\n3HEN3wKBgQDL+EBFqtlfRuU6iM6ZqXJg78a8aGJHyoDxEMWPo5pWBdB5Tkr4z27z\n5Yt+cbll6y7f+KllrOYjy1HgYtbW5gI+ECfhZHAkSEB9o5B//D4esZWAyB+Lrda3\nR6UiHesMtMx3o/OYtW+XkwrGhrnsnnCRglfJa2AUURBV7HZ6UP0GFg==\n-----END RSA PRIVATE KEY-----"
    status["install_cmd"] = "sudo apt update ; sudo apt -y install iperf3"

    get_dagster_logger().info(status)
    yield Output(status, output_name="status")

@op(required_resource_keys={"ssh_client"})
def iperf3_install_cmd_server(context, out1):
    ssh_client = context.resources.ssh_client
    ssh_client.run_sync_command(out1["username"], out1["host_server"], out1["install_cmd"])
    return True

@op(required_resource_keys={"ssh_client"})
def iperf3_install_cmd_client(context, out1):
    ssh_client = context.resources.ssh_client
    ssh_client.run_sync_command(out1["username"], out1["host_client"], out1["install_cmd"])
    return True

@op(out={"result": Out(dict)},required_resource_keys={"ssh_client", "kiwitcms"})
def run_iperf3(context, status, install_client_status, install_server_status):
    if install_client_status == None or install_server_status == None:
        return
    
    iperf3_protocols = status['iperf3_protocols']

    result = {}
    
    ssh_client = context.resources.ssh_client

    for protocol, protocol_data in iperf3_protocols.items():
        curr_protocol = protocol
        protocol = '-u' if protocol == 'UDP' else ''

        bandwidth = ''
        if protocol_data.get('bandwidth') != None:
            bandwidth = 'b' + protocol_data.get('bandwidth')   
        
        client_time_out = "-t " + str(protocol_data.get('client_timeout',10))     

        reverse = '-R' if protocol_data.get('reverse',False) else ''
        status["server_command"] = f"iperf3 -s -J -i1 {status['server_exp_addr']} > {status['server_output_path']} &"
        status["client_command"] = f"iperf3 -c {status['server_exp_addr']} -i1 {client_time_out} {bandwidth} {reverse} {protocol} -J > {status['client_output_path']}"

        index = ssh_client.runRemoteCommand(status["username"], status["host_server"], status["server_command"])
        if index == None:
            raise Exception("Unable to execute server command on remote server")

        ssh_client.run_sync_command(status["username"], status["host_client"], status["client_command"])
        get_client_result = json.loads(ssh_client.run_sync_command(status["username"], status["host_client"], status["get_client_output"]))
        get_server_result = json.loads(ssh_client.run_sync_command(status["username"], status["host_server"], status["get_server_output"]))
        result[curr_protocol] = {"iperf3_server_result": get_server_result, "iperf3_client_result": get_client_result}
        add_attachment(context, status, curr_protocol, json.dumps(result[curr_protocol]))

        index = ssh_client.runRemoteCommand(status["username"], status["host_server"], status["kill_command"])
        ssh_client.getRemoteResults(index)

    del status['server_command']
    del status['client_command']
    
    # addAttachment(context, status, "iperf3_result", result)
    update_execution(context, status, "execution_iperf", "PASSED")

    yield Output(result, output_name="result")

@op(required_resource_keys={"emulab_api"})
def terminate_iperf3_experiment(context, status, result):
    if not status["iperf3_expr_terminate"]:
        return True

    params = status
    portal = context.resources.emulab_api
    portal.terminateExperiment(action="experiment.terminate",
               name=params["name"],
               proj=params["proj"])

    context.log.info("Final Result : ")
    context.log.info(result)
    return True

@resource(config_schema={"creds": dict})
def ssh_client(context):
    creds = context.resource_config["creds"]
    return SSHClient(creds)

@resource(config_schema={"creds": dict})
def emulab_api(context):
    creds = context.resource_config["creds"]
    return EmulabBaseAction(creds)

@resource(config_schema={"creds": dict})
def kiwitcms(context):
    creds = context.resource_config["creds"]
    return KiwiTcms(creds)

resource_defs = {
    "emulab_api": emulab_api.configured({"creds": {"certificate" : "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAxUjjKKc0TnawGrujjxcgSdmS2XTHhuPoMhWtvyOHgpA0YfGz\nUqU1jSWTdIaZajbsqU93GmYO28FrcAvbolUGwdpmZoJ+Ykb8uGjk8o5/vevFX7Hn\nqm/3eh7mb2BsmhuNcT/XftH5caXcRAFUBIzAX5S62pVKIZYJQlGZuY92RpicLWHv\nl01+FXXWwbbP03Gcw4eJEWaOcqB971pKOb0xHXY6A8Q2+gi6q2UyyGFmfrQYna1U\ngi5+qObOrkpgf9NVNQUk6+KS0aCRIhM2K+TFCtAaU7+Bji4NUFTD6TV6G998F91x\nuNDP3eKe1HrU94mqtt7qS3oIAbmGWG2ZqWYjLQIDAQABAoIBAQCCCDSRSwPS46/X\nLu3zFk8V3e5IngwVAIq+2EMYxJZAnEOqeuCzDGlriuTR3RWpSGJchh/+ql4IwkfK\nJkaxjFFT3Fu6HTCoHhncgmbE6N+RkqEZHe2QDRSUzWEc7/Uk7hwgFPA4irVbDvNL\nb4StKwhw91rhT5z43idkKtyjzccrZ6rb56yxYmK/RyooqzoNQzpKNBPw+Bzvlsio\nfdp8KMdg6ZLBYWsTDyQYYAF3SzPcS9iOJMIikPXkN0vDa3FZcBq3L2c9wtyujLtd\ntY8O5CDyiUTE1PnErsb6JMqX5u2lmmmphEn2vsrB+IrdTj7AimPe+fuScPRMxBzl\nh6bMHvqRAoGBAOTcZUOmv62uGi3Ev/NRxJDT5Sq1JyCE/D0ytodUYIKe07Pg5ULy\nxHCwa9qxtgy1oXwgT4TyY+7HxJPPgh4XEGIRhzIO3GxZvnBekiz8hM8TUbhYQs0A\nO2OzLXZ3EHHJecL3u7ekcozehkM7fyxgSfu2jI8ErAI67BquNmwn6mhvAoGBANyt\n7D1qnwAE31Z+GxdvvykcC4n5Fe7LmgKt2yT0j9d43mIiFmweoK0Gx2iXjem7CmFa\n/DAw4NG0KBXfeXTZo/EIdGePhFiS/pZgvWL05a/OIzC1hUuNHW4VOauT8LRUJreb\n111S+JQFDej390vcTBphgUFjIVUiO88gB9XLmeQjAoGBAMDsgXjMAl0YnPzkFKO6\n1Cb6WP7tgrRJJpE4ROJD4K970B6cgOc02x/buK+8iyifnCeU3TPHWTKauD2Z+af/\n7VGeUQeDu5Ci/oSfVD4UCKi/Mm5iCL3jai0biEGLERO6sagHSfj+6YmStD/M0eHI\nsdU9B7QwTgf7P7kmDr6Rz2qFAoGAZ4CjdsLVRFsErTqEVl+xFwTyXIoZQWD5IRyN\n1i1mcbddBkXaomdTERRG3sWGaAO0AKl98tRZ/cEzs96Wxrl1bQ18Gz5y4mA/TCSf\nbxRRzj0uwB/DHwZWsVT/MXw3vzxg1Gkhf8H0mOgt+AClWk0+3Hiy+QoXjih6SNFL\nG3lc+mUCgYAtK/yNztPZBvNlRWLsXHNIMdvLtyUtmDW9I9N6Rk/PnoWIDnEmvq9O\nzVi2YFmbj+9JnVyE5X6rRJpToIGmhEB9a87nf2mZy54uSrpWQ4pXz8m1MH3Yiefq\n4L3W9DYCM0BeojJOhozcCAGapb/Ig5298OdcPr/GQmXEo47mn41DwQ==\n-----END RSA PRIVATE KEY-----\n-----BEGIN CERTIFICATE-----\nMIIEXDCCA8WgAwIBAgIDFNEBMA0GCSqGSIb3DQEBBQUAMIG1MQswCQYDVQQGEwJV\nUzENMAsGA1UECBMEVXRhaDEXMBUGA1UEBxMOU2FsdCBMYWtlIENpdHkxHTAbBgNV\nBAoTFFV0YWggTmV0d29yayBUZXN0YmVkMR4wHAYDVQQLExVDZXJ0aWZpY2F0ZSBB\ndXRob3JpdHkxGDAWBgNVBAMTD2Jvc3MuZW11bGFiLm5ldDElMCMGCSqGSIb3DQEJ\nARYWdGVzdGJlZC1vcHNAZW11bGFiLm5ldDAeFw0yMzAxMjQxODAzMDBaFw0yNTEw\nMjAxODAzMDBaMIGiMQswCQYDVQQGEwJVUzENMAsGA1UECBMEVXRhaDEdMBsGA1UE\nChMUVXRhaCBOZXR3b3JrIFRlc3RiZWQxEjAQBgNVBAsTCXNzbHhtbHJwYzEtMCsG\nA1UEAxMkOTYxZjZiNjgtOWJhMi0xMWVkLWIzMTgtZTQ0MzRiMjM4MWZjMSIwIAYJ\nKoZIhvcNAQkBFhNhc2hvazE1N0BlbXVsYWIubmV0MIIBIjANBgkqhkiG9w0BAQEF\nAAOCAQ8AMIIBCgKCAQEAxUjjKKc0TnawGrujjxcgSdmS2XTHhuPoMhWtvyOHgpA0\nYfGzUqU1jSWTdIaZajbsqU93GmYO28FrcAvbolUGwdpmZoJ+Ykb8uGjk8o5/vevF\nX7Hnqm/3eh7mb2BsmhuNcT/XftH5caXcRAFUBIzAX5S62pVKIZYJQlGZuY92Rpic\nLWHvl01+FXXWwbbP03Gcw4eJEWaOcqB971pKOb0xHXY6A8Q2+gi6q2UyyGFmfrQY\nna1Ugi5+qObOrkpgf9NVNQUk6+KS0aCRIhM2K+TFCtAaU7+Bji4NUFTD6TV6G998\nF91xuNDP3eKe1HrU94mqtt7qS3oIAbmGWG2ZqWYjLQIDAQABo4IBBTCCAQEwDAYD\nVR0TAQH/BAIwADAdBgNVHQ4EFgQUi+DeLkjbdbTcr/eeIqXEUr/UZ4wweAYDVR0R\nBHEwb4YpdXJuOnB1YmxpY2lkOklETitlbXVsYWIubmV0K3VzZXIrYXNob2sxNTeB\nE2FzaG9rMTU3QGVtdWxhYi5uZXSGLXVybjp1dWlkOjk2MWY2YjY4LTliYTItMTFl\nZC1iMzE4LWU0NDM0YjIzODFmYzBYBggrBgEFBQcBAQRMMEowSAYUaYPMk4ComMyo\nx72xp4CAqq7XihuGMGh0dHBzOi8vd3d3LmVtdWxhYi5uZXQ6MTIzNjkvcHJvdG9n\nZW5pL3htbHJwYy9zYTANBgkqhkiG9w0BAQUFAAOBgQC7vI7c9drl9UnpkYJrJ6a2\nun72jYQQ+mM8/fu/ojmDmtOQDOrFgnz0SngCAxZwzi3+AJbCBbuS/6tv9+2Dnx4+\nZj3zgcOcWRN9wcxj0CTlETV6ZWvxqsHvKMpaD+CgYrxH+jus9D7l04tCXk/M0mN9\nHG2voWwyt0AIteb21/f1sA==\n-----END CERTIFICATE-----",
    "sshpubkey"   : "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiMFbRGIFdmC8JSHWaqruH3AqtCCkyHEFd19Y7HnSOH91q1VQ+LkBvum29t7j2Q1QhVEBL0my2Molrtf0u4U0AbO5m7j5ljfXSkTi0R7BviZY1pBlAjstdSdntbS9IH0HnY+tnS1nc/PEGf4IyDpFwRRdzJvKIESZwqCtij4fvJZ8rThqCSfUAQcCusdR70TK8xX0wuzdMf3/yQ2TSdFsAdnm0jfxJlLkDdW92zESdzOdU5tAAPgnCwJMKM7gQR+wOzn8KL2jraLrGVcNxhjjL0EvqgmazBkx33eb1l90RbacUiY3gMrzx4CZNgQNk9IjXQ4C/OahKiDCkWEb/+eZl",
    "debug"       : False
    }}),
    "ssh_client" : ssh_client.configured({"creds": {
    "private_key"   : "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAojBW0RiBXZgvCUh1mqq7h9wKrQgpMhxBXdfWOx50jh/datVU\nPi5Ab7ptvbe49kNUIVRAS9JstjKJa7X9LuFNAGzuZu4+ZY310pE4tEewb4mWNaQZ\nQI7LXUnZ7W0vSB9B52PrZ0tZ3PzxBn+CMg6RcEUXcybyiBEmcKgrYo+H7yWfK04a\ngkn1AEHArrHUe9EyvMV9MLs3TH9/8kNk0nRbAHZ5tI38SZS5A3VvdsxEncznVObQ\nAD4JwsCTCjO4EEfsDs5/Ci9o62i6xlXDcYY4y9BL6oJmswZMd93m9ZfdEW2nFImN\n4DK88eAmTYEDZPSI10OAvzmoSogwpFhG//nmZQIDAQABAoIBADgwiRNd6wuy+rC7\nosrX1aqG6Ef0KNmMgnTxnu3Sak0G6x3lFI+KVJagvv5YVUM0JkR5vODH4RPqREPF\nxBZmxdec5adRaNqkKQMvfrauk4jA4QJqfLkp/3itakvgZN4Wvi1dJ6QcreBq3VIE\nAOY4t9huh2ZEmwfa8Tdce9NkqJKIKAFMNj7ZNJffzP9xkmYy0YPG7CJKNLPAD7LP\nU8mfn6LOZ+qJtjDu/hLUjeo/0a7PGwyvESNFhlmbfiEahmvNchDzNHK5ony6cB9p\nZzP/hz8YZxnRh5QfRU4jOWvLQa0wyB+e0t0ScLGvfY6PUz0TQFB9Qz2r9tymrHkH\nrwuAauECgYEAy9ejWYGvIWV0xjpjiSLXhJ5zteMcYCeS9ZxqdoCzrrBZs+zAIQ2J\nL9ANwpKtqqABqgt506m8z8tfFwP9qEYqc468hzbh906ujZL3/KD9SoIYt367z5th\nJ8kkhI44aICHKOs+sdZHJ4fAml5QuVpbne9BCXL1a8dAuFxONUAMJz0CgYEAy7BB\nWT+pOypFcUG7FswRjXtSiPMlSMCLfa+qBR44lJAWKEHlAdJfi9dWTaoLdQ76vibY\nBZF/6VkmBdf/p1VxQc/Tqz79VQmCgIZjzUVKgma4chvIjxGGQZCI7eA+utaevwLH\nBrRoJy1NtDVPOvCJAQ2sThxj75Cfb7c4iHCo7kkCgYBgO+GmdA8WGSvUGeS77XMJ\n7TR7fkj7LDO5OKsaXxB1PFRdGTxrXlSR+gtE/LJ4hynB+cD+KHg3MbAm0X+DXfzU\nqWJgA40SkFbgB66D2g06i8jXeEF6dcScX92ZpvsCewtUShtDIgJdqz1Go5hAqWPv\nX/C4YUl0JJFteEaD5UTXkQKBgQCRhb1KbCcV9B9kyRdHNEieEitnWH+cuKNwCl6o\nBW1bBo4t+NYKNSi6GgC4v/IrkaYm0E2pvMfsCRsXNbia7npaEIcjY39Oj9rCxEDt\nWMHg5E6Gh78EJ/sJQJ/zgzCN/+Ouh+d1rNJ0mvNW+nc+nrbjH8eojrP/Zg3inL4x\nDHxEsQKBgByynvGZ1wplwSImDPJwpKHDQywTfs1bn+CZoFkQocB8M/k34xBne2MR\nfnLuEWzAAvV2x2kIwm1CtcRVJk28MdClWDCXRldwWwTDRTNAMnX2PPWAxvOcmaDO\nIb8bLnEHpPb0zuJzo6bE1J6um8bauws+TbzO4WDXQAtUict2VlAn\n-----END RSA PRIVATE KEY-----",
    "conn_timeout"  : 10,
    "look_for_keys" : False,
    "allow_host_key_change": True
}}),
    "kiwitcms"  : kiwitcms.configured({"creds" :{
    "url"      : "https://node-0.ashok-dagster.PowderSandbox.emulab.net:8444/xml-rpc/",
    "username" : "admin",
    "password" : "9c738abd5be3"
}})
}

input_params = {
    "bindings": {"nodes": 2, "numberLans": 1},
    "name": "test-bandwidth",
    "profile": "emulab-ops,dijon-git",
    'proj' : "PowderSandbox",
    'username' : "ashok157",
    "wait_for_status": True,
    "iperf3_version": "1",
    "iperf3_protocols": {
        "TCP": {
            # "bandwidth": None,
            # "reverse": True
            "client_timeout": 20
        },
        "UDP": {
            # "bandwidth": None,
            # "reverse": True,
            "client_timeout" : 20
        }
    }
}

@job(
    config = {
        "ops" : {
            "start_run" : {
                "config" : {
                    "params" : input_params
                }
            },
        },
    },
    resource_defs = resource_defs)
def run_iperf3_test():
    status = start_run()
    status = setup_tcms(status)
    params = experiment_status(start_experiment(status), status)
    params = build_config(params)
    install_server_status = iperf3_install_cmd_server(params)
    install_client_status = iperf3_install_cmd_client(params)
    result = run_iperf3(params, install_server_status, install_client_status)
    terminate_iperf3_experiment(params, result)