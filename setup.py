#!/usr/bin/env python

from setuptools import setup, find_packages

"""setup(
    name="dagster-iperf3",
    version="0.2",
    author="Ashok Vengala",
    author_email="u1419414@utah.edu",
    url="https://gitlab.flux.utah.edu/ashok157/dagster-iperf3",
    description="Iperf3 Module",
    package_dir={"": "src"},
    packages=find_packages(where="src")
) """

setup( 
    name='dagster-iperf3', 
    version='0.1', 
    description='Measure Iperf3 bandwidth', 
    author='Ashok Vengala', 
    author_email='u1419414@utah.edu', 
    packages=['dagster-iperf3'] 
) 
